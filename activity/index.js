// console.log("Hello World!");


let userDetails = function inputUserDetails(){
	let yourFullName = prompt('What is your name?');
	let yourAge = prompt('How old are you?');
	let yourLocation = prompt('Where do you live?');
	alert("Thank you for your input.");

	console.log('Hello, ' + yourFullName + '!');
	console.log('You are ' + yourAge + ' years old.');
	console.log('You live in ' + yourLocation + '.');
};

userDetails();

let favoriteArtists = function getFavoriteArtists(){
	let favoriteArtists = ['1. Black Pink', '2. Ariana Grande', '3. Adele', '4. Sarah Geronimo', '5. Julie Anne San Jose']
	console.log(favoriteArtists[0]);
	console.log(favoriteArtists[1]);
	console.log(favoriteArtists[2]);
	console.log(favoriteArtists[3]);
	console.log(favoriteArtists[4]);
};

favoriteArtists();

let favoriteMovies = function getFavoriteMovies(){
	let favoriteMovies = ["1. Avengers: Infinity War", "2. Avengers: Endgame", "3. Spider-Man: No Way Home", "4. Harry Potter and the Sorcerer's Stone", "5. Harry Potter and the Prisoner of Azkaban"]
	let movieRatings = ["85%", "94%", "93%", "81%", "90%"]
	console.log(favoriteMovies[0]);
	console.log("Rotten Tomatoes Rating: " + movieRatings[0])
	console.log(favoriteMovies[1]);
	console.log("Rotten Tomatoes Rating: " + movieRatings[1])
	console.log(favoriteMovies[2]);
	console.log("Rotten Tomatoes Rating: " + movieRatings[2])
	console.log(favoriteMovies[3]);
	console.log("Rotten Tomatoes Rating: " + movieRatings[3])
	console.log(favoriteMovies[4]);
	console.log("Rotten Tomatoes Rating: " + movieRatings[4])

}

favoriteMovies();



// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	
};

printFriends();

// console.log(friend1);
// console.log(friend2);

/*
   - Create a function which is able to gather user details using prompt(). 
   - Create a function which is able to display simple data in the console.
   - Apply best practices in creating and defining functions by debugging erroneous code.

   Activity Output:
    1. prompt() and alert() used to show more interactivity in page.
	2.Values logged in console after invoking the function for objective  no.1

		Note: Name your own functions and variables but follow the conventions and best practice in naming functions and variables.
	3. Values shown in the console after invoking function created for objective 2.
	4. Values shown in the console after invoking function created for objective 3.
	5. Values shown in the console after invoking function debugged for objective 4.

	Activity Instruction:
	1. In the S17 folder, create an activity folder, an index.html file inside of it and link the index.js file.
	2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
	3. Copy the activity code from your Boodle Notes. Paste the activity code from your Boodle Notes to your index.js file.
	4.  Create a function which is able to prompt the user to provide their full name, age, and location. 
		- use prompt() and store the returned value into function scoped variables within the function. 
		- show an alert to thank the user for their input.
		- display the user's inputs in messages in the console.
 		- invoke the function to display the user’s information in the console.
		- follow the naming conventions for functions.
	5. Create a function which is able to print/display your top 5 favorite bands/musical artists. 
		- invoke the function to display your information in the console.
		- follow the naming conventions for functions.
	6. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating. 
		- look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
        - invoke the function to display your information in the console.
		- follow the naming conventions for functions.
	7. Debugging Practice - Debug the given codes and functions to avoid errors.
		- check the variable names.
		- check the variable scope.
		- check function invocation/declaration.
		- comment out unusable codes.
	8. Create a git repository named S17.
 	9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	10. Add the link in Boodle.

	How you will be evaluated
	1. No errors should be logged in the console.
	2. prompt() is used to gather information.
	3. alert() is used to show information.
	4. All values must be properly logged in the console.
	5. All variables are named appropriately and defines the value it contains.
	6. All functions are named appropriately and follows naming conventions.

 */